Title: Play/Pause an audio clip using accelerometer for ios.
Group NO. and Group members: 
Group No.: 19
Group Members:
1. Dipankar Ghosh
2. Archana Narayanan

Sensors involved in the project:
Accelerometer

Main goal (paragraph):
The main target of this project is to create an app in ios which uses accelerometer as a sensor to sense hand motion and is used to pay/pause an audio when  a particular hand motion is sensed. 
Application scenario design (paragraph):
•	Initial phase – To include a music library or an audio clip.
•	Secondary phase – Include the accelerometer to recognize the hand gesture for play/pause.
•	Tertiary phase – Combining both and running them together.
•	Final phase – Testing.


